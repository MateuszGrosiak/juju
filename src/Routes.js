import React, {Component} from 'react';
import { BrowserRouter, Route } from 'react-router-dom'
import {withStyles} from 'material-ui/styles';

import Application from './Application';
import styles from './styles';
import Home from './components/Home';
import BusinessCalculator from './components/BusinessCalculator';

class Routes extends Component {

    render() {
        const {classes} = this.props;
        return (
            <BrowserRouter>
                <Application classes={classes}>
                    <Route exact path="/"
                        render={props => (<Home {...props} classes={classes}/>)}
                    />
                    <Route path="/b2b" 
                        render={
                            props => (<BusinessCalculator {...props} classes={classes} />)
                        }
                    />
                </Application>
            </BrowserRouter>
        );
    }
}

export default withStyles(styles, {withTheme: true})(Routes);
