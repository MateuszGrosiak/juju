import React from 'react';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';

export default function Home(props) {
    const {classes} = props;
    return(
        <Grid item xs={11}>
            <div className={classes.panel}>
                <Typography variant="headline" component="h3">
                    Pierwszy kalkulator dochodów na świecie!
                </Typography>
            </div>
        </Grid>
    );
}