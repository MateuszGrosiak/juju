import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import Drawer from 'material-ui/Drawer';
import Divider from 'material-ui/Divider';
import Hidden from 'material-ui/Hidden';
import List, {ListItem, ListItemText} from 'material-ui/List';
import BusinessIcon from 'material-ui-icons/Business';
import HomeIcon from 'material-ui-icons/Home';
import ListItemIcon from 'material-ui/List';

class SideMenu extends Component {

    drawerContent = (classes) => {
        return (
            <div>
                <div className={classes.toolbar} />
                <Divider/>
                <List>
                    <Link to="/" style={{textDecoration: 'none'}}>
                        <ListItem button onClick={this.props.handleDrawerToggle}>
                            <ListItemIcon>
                                <HomeIcon/>
                            </ListItemIcon>
                            <ListItemText primary="Dom"/>
                        </ListItem>
                    </Link>
                    <Link to="/b2b" style={{textDecoration: 'none'}}>
                        <ListItem button onClick={this.props.handleDrawerToggle}>
                            <ListItemIcon>
                                <BusinessIcon/>
                            </ListItemIcon>
                            <ListItemText primary="Kalkulator B2B"/>
                        </ListItem>
                    </Link>
                </List>
            </div>
        );
    }

    render() {
        const {classes} = this.props;
        return (
            <div>
                <Hidden mdUp>
                    <Drawer
                        variant="temporary"
                        anchor={'left'}
                        open={this.props.mobileOpen}
                        onClose={this.props.handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {this.drawerContent(classes)}
                    </Drawer>
                </Hidden>
                <Hidden smDown implementation="css">
                    <Drawer
                        variant="permanent"
                        open
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                    >
                        {this.drawerContent(classes)}
                    </Drawer>
                </Hidden>
            </div>
        );
    }
}

export default SideMenu;