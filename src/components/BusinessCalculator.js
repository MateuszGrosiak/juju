import React, {Component} from 'react';
import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import Radio, { RadioGroup } from 'material-ui/Radio';
import { FormLabel, FormControl, FormControlLabel } from 'material-ui/Form';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';

const ZUS = {
    lowered: {
        health: 319.94,
        sickness: 15.44,
        social: 184.72
    },
    normal: {
        health: 319.94,
        sickness: 15.44,
        social: 184.72
    },
    deduction: 275.51
}

const contributions = {
    lowered: 'lowered',
    normal: 'normal'
}

const settlingOptions = {
    linear: 'linear',
    scale: 'scale',
    lumpSum: 'lumpSum'
};

export default class BusinessCalculator extends Component {
    state = {
        settling: settlingOptions.linear,
        contribution: contributions.normal,
        costs: 0,
        labourFund: 0,
        netto: 0,
        actualProfit: null
    }

    constructor(props) {
        super(props);

        this.handleSettlementChange = this.handleSettlementChange.bind(this);
        this.handleContributionChange = this.handleContributionChange.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.calculateProfit = this.calculateProfit.bind(this);
    }

    handleSettlementChange(event, value) {
        this.setState({settling: value});
    }

    handleContributionChange(event, value) {
        this.setState({contribution: value});
    }

    handleTextChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    calculateProfit() {
        const {netto, costs} = this.state;
        //test: liniowy, niski zus
        const revenue = netto;
        const income = revenue - costs;//... - loss from previous years (whatever that means)
        const incomeTaxationBase = Math.round(income - ZUS.lowered.social);// ... + sickness?
        const tax = Math.round(incomeTaxationBase * 0.19 - ZUS.lowered.health);
        const actualProfit = revenue - (ZUS.lowered.health + ZUS.lowered.social + ZUS.lowered.sickness + tax);
        this.setState({actualProfit});
    }

    render() {
        const {classes} = this.props;
        return (
            <Grid item xs={11}>
                <div className={classes.panel}>
                    <Typography variant="title" align="left">
                        Kalkulator - Ile zarobię na fakturze?
                    </Typography>
                    <Paper style={{marginTop: 20}} align="left">
                        <FormControl component="fieldset" required className={classes.formControl}>
                            <FormLabel component="legend">Składka ZUS</FormLabel>
                            <RadioGroup
                                name="contribution"
                                className={classes.group}
                                value={this.state.contribution}
                                onChange={this.handleContributionChange}
                            >
                                <FormControlLabel value={contributions.lowered} control={<Radio />} label="obniżona" />
                                <FormControlLabel value={contributions.normal} control={<Radio />} label="pełna" />
                            </RadioGroup>
                        </FormControl>
                        <FormControl component="fieldset" required className={classes.formControl}>
                            <FormLabel component="legend">Sposób rozliczania</FormLabel>
                            <RadioGroup
                                name="settling"
                                className={classes.group}
                                value={this.state.settling}
                                onChange={this.handleSettlementChange}
                            >
                                <FormControlLabel value={settlingOptions.linear} control={<Radio />} label="podatek liniowy" />
                            </RadioGroup>
                            <TextField
                                required
                                id="netto"
                                label="Kwota netto"
                                value={this.state.netto}
                                onChange={this.handleTextChange('netto')}
                                className={classes.textField}
                                margin="normal"
                            />
                            <TextField
                                id="costs"
                                label="Koszty"
                                value={this.state.costs}
                                onChange={this.handleTextChange('costs')}
                                className={classes.textField}
                                margin="normal"
                            />
                            <Button 
                                variant="raised" 
                                color="primary" 
                                className={classes.button} 
                                style={{marginTop: 20}}
                                onClick={this.calculateProfit}
                            >
                                Oblicz
                            </Button>
                            <Divider style={{marginTop: 20, marginBottom: 20}}/>
                            {
                                this.state.actualProfit !== null 
                                    ?   <Typography variant="title" align="left" color="primary">
                                            Na rękę: <b>{this.state.actualProfit} zł</b>
                                        </Typography>
                                    :   null
                            }
                        </FormControl>
                    </Paper>
                </div>  
            </Grid>
        );
    }
}