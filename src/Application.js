import React, {Component} from 'react';
import {Switch} from 'react-router-dom';
import Grid from 'material-ui/Grid';

import TopMenu from './components/TopMenu';
import SideMenu from './components/SideMenu';

export default class Application extends Component {

    state = {
        mobileOpen: false
    }

    handleDrawerToggle = () => {
        this.setState({
            mobileOpen: !this.state.mobileOpen
        });
    };

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <TopMenu classes={classes} handleDrawerToggle={this.handleDrawerToggle}/>
                <SideMenu
                    classes={classes}
                    mobileOpen={this.state.mobileOpen}
                    handleDrawerToggle={this.handleDrawerToggle}/>
                <Grid
                    container
                    spacing={24}
                    style={{
                    paddingTop: 80
                }}
                    justify="center">
                    <Switch>
                        {this.props.children}
                    </Switch>
                </Grid>
            </div>
        );
    }
}